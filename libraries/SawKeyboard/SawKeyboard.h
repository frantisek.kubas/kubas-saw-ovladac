#ifndef __SAWKEYBOARD_H__
#define __SAWKEYBOARD_H__

#include <Arduino.h>

class SawKeyboard {
  #define COLUMNS 4
  #define ROWS    4
  
  public:
    SawKeyboard( int column1pin,int column2pin,int column3pin,int colum4npin,
              int row1pin,int row2pin,int row3pin,int row4pin);
    ~SawKeyboard();
    uint8_t readKeys();//read keys
	uint8_t getKey(); //read and get key (one key reterned only once while one pressed)
	uint8_t getChar(); //read and get key (first pressed)
	uint8_t getFirstKey(); //returned first key but not read
	uint8_t getSecondKey(); //returned second key but not read
	
	uint8_t getRowOf(uint8_t character);
	uint8_t getCollumnOf(uint8_t character);
    
  private:
	uint8_t shiftPressedchars(uint8_t pressedChar);
	bool stateOfKey(uint8_t key);
  
  
	uint8_t keys[ROWS][COLUMNS];
    bool pressedMatrix[ROWS][COLUMNS];
    bool lastPressedMatrix[ROWS][COLUMNS];
    uint8_t colPins[COLUMNS];
    uint8_t rowPins[ROWS];
	uint8_t pressedKeys[2];
	
	uint8_t key;
};
#endif // __SAWKEYBOARD_H__