#include "SawKeyboard.h"

SawKeyboard::~SawKeyboard(){}

SawKeyboard::SawKeyboard( int column1pin,int column2pin,int column3pin,int column4pin,
                    int row1pin,int row2pin,int row3pin,int row4pin)
{
	uint8_t keysChars [ROWS][COLUMNS] = {
		{'A','1','2','3'},
		{'B','4','5','6'},
		{'C','7','8','9'},
		{'D','*','0','#'}
	};

	//set pins
	this->colPins[0] = column1pin;
	this->colPins[1] = column2pin;
	this->colPins[2] = column3pin;
	this->colPins[3] = column4pin;
	this->rowPins[0] = row1pin;
	this->rowPins[1] = row2pin;
	this->rowPins[2] = row3pin;
	this->rowPins[3] = row4pin; 
	//init state matrixs
	for(int row=0; row < ROWS; row++){
		for(int col=0; col < COLUMNS; col++) {
			this->pressedMatrix[row][col]=false;
			this->lastPressedMatrix[row][col]=false;
			this->keys[row][col] = keysChars[row][col];
		}
	}	  

  for (int row = 0; row < ROWS; row++) {
    pinMode(this->rowPins[row], INPUT_PULLUP);
    digitalWrite(this->rowPins[row], HIGH);
  }
  
  pressedKeys[0]=0;
  pressedKeys[1]=0;
  this->key=0;
}

uint8_t SawKeyboard::readKeys()
{
	for(int row=0; row < ROWS; row++){
		for(int col=0; col < COLUMNS; col++) {
			this->lastPressedMatrix[row][col]=this->pressedMatrix[row][col];
		}
	}	
	bool state;
	uint8_t countPressedkeys = 0;
	for (int col = 0; col < COLUMNS; col++) {
		// Set the column mode to OUTPUT
		pinMode(this->colPins[col], OUTPUT);
		// Set the column LOW
		digitalWrite(this->colPins[col], LOW);
		for (byte row = 0; row < ROWS; row++) {
		// Read each row pin, store the inverted value locally
		state = !digitalRead(this->rowPins[row]);
		if (state) {
				countPressedkeys++;
		}
		this->pressedMatrix[row][col] = !digitalRead(this->rowPins[row]);
		}
		// Set the column HIGH 
		digitalWrite(this->colPins[col], HIGH);
		// Set the column mode to INPUT
		pinMode(this->colPins[col], INPUT);
	}
	// End of problem section
	if(countPressedkeys >2) { // if more than 2 keys is pressed
	//	TO DO BEHAVIOUR		
		return countPressedkeys; //return and keep old pair
		//return and null old pairs
	}
	for (byte row = 0; row < ROWS; row++) {
		for (byte col = 0; col < COLUMNS; col++) {
			if (this->pressedMatrix[row][col] == 1) {
				shiftPressedchars(this->keys[row][col]);
			}
		}
	}
/*	if (countPressedkeys == 0) {
		pressedKeys[0] = 0;
		pressedKeys[1] = 0;
	} else if (countPressedkeys == 1) {
		shiftPressedchars(0);
	}*/
	if (countPressedkeys < 2) {
		shiftPressedchars(0);
	}
	
			/*	Serial.println();
				Serial.print("keys: [");
				Serial.print((char)(this->pressedKeys[0] == 0 ? ' ' : this->pressedKeys[0]));
				Serial.print("],[");
				Serial.print((char)(this->pressedKeys[1] == 0 ? ' ' : this->pressedKeys[1]));
				Serial.println("]");
			*/
	return countPressedkeys;		
}

uint8_t SawKeyboard::getRowOf(uint8_t character)
{
	for (byte row = 0; row < ROWS; row++) {
		for (byte col = 0; col < COLUMNS; col++) {
			if (character == this->keys[row][col]) {
				return row;
			}
		}
	}
	return 255;
}

uint8_t SawKeyboard::getCollumnOf(uint8_t character)
{
	for (byte row = 0; row < ROWS; row++) {
		for (byte col = 0; col < COLUMNS; col++) {
			if (character == this->keys[row][col]) {
				return col;
			}
		}
	}
	return 255;
}

bool SawKeyboard::stateOfKey(uint8_t key) {
	uint8_t row = getRowOf(key);
	uint8_t collumn = getCollumnOf(key);
	if(row > ROWS || collumn > COLUMNS) return false;
	return this->pressedMatrix[row][collumn];
}

uint8_t SawKeyboard::shiftPressedchars(uint8_t pressedChar)
{
	if(stateOfKey(this->pressedKeys[0])){ //first is still pressed
		if(stateOfKey(this->pressedKeys[1]) || this->pressedKeys[0] == pressedChar) {//both is still pressed or pressed is first
			return;
		} 
		this->pressedKeys[1] = pressedChar;
		return;
	}
	//first possition is empty...
	this->pressedKeys[0] = this->pressedKeys[1]; //shift second to first
	this->pressedKeys[1] = 0;
	
	if (stateOfKey(this->pressedKeys[0])){ //if second is still pressed
		if(this->pressedKeys[0] == pressedChar) //if second is new pressed do nothing
			return;
		this->pressedKeys[1] = pressedChar;	// if sesond is still presed ... shift them and newpressed is new second
		return;
	}
	//neither of the originals is compressed
	this->pressedKeys[0] = pressedChar; //if second is new pressed
	return;
}

//to do
uint8_t SawKeyboard::getKey() //read and get key (one key reterned only once while one pressed)
{
	if (readKeys() == 0){
		this->key = 0;
	} else if(this->key != this->pressedKeys[0]) {
			//keyPocessed = KeyStatus::nonProcessed
			this->key = this->pressedKeys[0];
			return this->key; 
	} 
	return 0; 
}

uint8_t SawKeyboard::getChar() //read and get key (first pressed)
{
	readKeys();
	return this->pressedKeys[0];
}

uint8_t SawKeyboard::getFirstKey() //returned first key but not read
{
	return this->pressedKeys[0];
}

uint8_t SawKeyboard::getSecondKey() //returned second key but not read
{
	return this->pressedKeys[1];
}

