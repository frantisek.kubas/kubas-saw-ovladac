#include "SawDisplay.h"

/*SawDisplay::SawDisplay (uint8_t sck, uint8_t mosi, uint8_t cs)
{
	//U8GLIB_ST7920_128X64_1X u8g(sck, mosi, cs);  // SPI Com: SCK = en = 12, MOSI = rw = 11, CS = di = 10
	this->display = new U8GLIB_ST7920_128X64_1X(sck, mosi, cs);
}*/


SawDisplay::SawDisplay(MyMenu * myMenu, StatusPanel * statusPanel, uint8_t sck, uint8_t mosi, uint8_t cs)
{
	//U8GLIB_ST7920_128X64_1X u8g(sck, mosi, cs);  // SPI Com: SCK = en = 12, MOSI = rw = 11, CS = di = 10
	this->display = new U8GLIB_ST7920_128X64_1X(sck, mosi, cs);
	this->myMenu = myMenu;
	this->statusPanel = statusPanel;
}

SawDisplay::~SawDisplay() 
{
	delete display;
}

uint8_t SawDisplay::draw(){
	// graphic commands to redraw the complete screen should be placed here  
	//Serial.println("drawing");
	this->display->setFont(u8g_font_unifont);
	this->display->firstPage();  
	do {
		if(this->myMenu->getVisible()) {
			drawMenu();
		}
		
		if(this->statusPanel->getVisible()){
			drawStatusPanel();
		}
	} while (this->display->nextPage());
}

void SawDisplay::drawMenu(){
	//myMenu->printStatus();
	this->display->drawVLine(myMenu->getLeft(), myMenu->getTop(), myMenu->getHeight());
	
	this->display->setPrintPos(myMenu->getTextXpos(), myMenu->getTextYposOfLine(1));
	this->display->print(myMenu->getMenu1().c_str());//this->display->print((char)key);
	this->display->drawFrame(myMenu->getLeft(), myMenu->getRowLineYposOfLine(1),myMenu->getWidth(),2);
	
	this->display->setPrintPos(myMenu->getTextXpos(), myMenu->getTextYposOfLine(2));
	this->display->print("B");//this->display->print((char)key);
	this->display->drawFrame(myMenu->getLeft(), myMenu->getRowLineYposOfLine(2),myMenu->getWidth(),2);
	
	this->display->setPrintPos(myMenu->getTextXpos(), myMenu->getTextYposOfLine(3));
	this->display->print("M");//this->display->print((char)key);
	this->display->drawFrame(myMenu->getLeft(), myMenu->getRowLineYposOfLine(3),myMenu->getWidth(),2);
	
	this->display->setPrintPos(myMenu->getTextXpos(), myMenu->getTextYposOfLine(4));
	this->display->print("mr");//this->display->print((char)key);
	
	if(myMenu->isPressed()) {
		this->display->drawFrame(myMenu->getPressedFrameXpos(), myMenu->getPressedFrameYpos(),myMenu->getPressedFrameWidth(),myMenu->getPressedFrameHeight());
	}
}

void SawDisplay::drawStatusPanel(){
	
	if(this->statusPanel->getSignalVisible()) {
		uint8_t centerX = 11;
		uint8_t centerY = 3;
		if(this->statusPanel->getSignalPower()){
			this->display->drawPixel(centerX, centerY);
			this->display->drawVLine(centerX-1, centerY-1, 3);
			this->display->drawVLine(centerX+1, centerY-1, 3);
			if(this->statusPanel->getSignalPower() > 25){
				this->display->drawPixel(centerX-3, centerY);
				this->display->drawPixel(centerX+3, centerY);
			}
			if(statusPanel->getSignalPower() > 50){
				this->display->drawVLine(centerX-5, centerY-1, 3);
				this->display->drawVLine(centerX+5, centerY-1, 3);
			}
			if(statusPanel->getSignalPower() > 70){
				this->display->drawVLine(centerX-7, centerY-2, 5);
				this->display->drawVLine(centerX+7, centerY-2, 5);
			}
			if(statusPanel->getSignalPower() > 90){
				this->display->drawVLine(centerX-9, centerY-2, 5);
				this->display->drawVLine(centerX+9, centerY-2, 5);
			}
		} else {
			this->display->drawHLine(centerX-7, centerY, 15);
		}
	}
	
	if(this->statusPanel->getBatteryVisible()) {
		uint8_t left = 28;
		uint8_t top = 1;
		this->display->drawFrame(left, top,22,5);
		this->display->drawFrame(50, top+1, 2,3);
		for(int i=1; i<=20;i++){
			if(i*5 > this->statusPanel->getBatteryPower()) {
				break;
			}
			this->display->drawVLine(left+i, top+1, 3);
		}
	}
}

uint8_t SawDisplay::drawMainFrame(){
	
}


//----------------------------------------------------------------


		MyMenu::MyMenu(){
			this->visible = true;
			this->collumns = 2;
			this->margins = 2;
			this->fontPixelHeight = 10;
			this->fontPixelWidth = 6;
			this->rowPixelHeight = 16;
			this->collumnPixelWidth = 8;
			this->pressedRow = 0;
			
			this->menu1= String("X");
		}
		
		MyMenu::~MyMenu(){
			
		}
		
		
		void MyMenu::setVisible(bool visible){
			this->visible = visible;
		}
		
		bool MyMenu::getVisible(){
			return this->visible;
		}
		
		void MyMenu::setCollumns(uint8_t collumns){
			if(collumns < MAX_MENU_COLLUMNS){
				this->collumns = collumns;
			} else {
				this->collumns = MAX_MENU_COLLUMNS-1;
			}
		}
		
		void MyMenu::setMenu1(String menu){
			this->menu1 = menu;
		}
		
		void MyMenu::setMenu2(String menu){
			this->menu2 = menu;
		}
		
		void MyMenu::setMenu3(String menu){
			this->menu3 = menu;
		}
		
		void MyMenu::setMenu4(String menu){
			this->menu4 = menu;
		}
		
		void MyMenu::setMenu(String menu1, String menu2, String menu3, String menu4, uint8_t collumns=2){
			this->menu1 = menu1;
			this->menu2 = menu2;
			this->menu3 = menu3;
			this->menu4 = menu4;
			setCollumns(collumns);
		}
		
		String MyMenu::getMenu1(){
			return this->menu1;
		}
		
		String MyMenu::getMenu2(){
			return this->menu2;
		}
		
		String MyMenu::getMenu3(){
			return this->menu3;
		}
		
		String MyMenu::getMenu4(){
			return this->menu4;
		}
		
		
		uint8_t MyMenu::getLeft(){
			//printStatus();
			return DISPLAY_WIDTH - (this->collumns * this->collumnPixelWidth + this->margins +1);
		}
		
		uint8_t MyMenu::getTop(){
			return this-> margins;
		}
		
		uint8_t MyMenu::getRight(){
			return DISPLAY_WIDTH - this->margins;
		}
		
		uint8_t MyMenu::getBottom(){
			return DISPLAY_HEIGHT - this->margins;
		}
		
		uint8_t MyMenu::getHeight(){
			return getBottom() - getTop();
		}
		
		uint8_t MyMenu::getWidth(){
			return getRight() - getLeft();
		}
		
		uint8_t MyMenu::getCollumns(){
			return this->collumns;
		}
		
		uint8_t MyMenu::getTextXpos(){
			return getLeft() + this->margins;
		}
		
		uint8_t MyMenu::getTextYposOfLine(uint8_t row){
			return getTop() + 1 + this->fontPixelHeight + (row-1) * this->rowPixelHeight;
		}
		
		uint8_t MyMenu::getRowLineYposOfLine(uint8_t row){
			return getTextYposOfLine(row) + this->margins;
		}
		
		bool MyMenu::isPressed(){
			return this->pressedRow;
		}
		
		uint8_t MyMenu::getPressedFrameXpos(){
			return getLeft()+1;
		}
		
		uint8_t MyMenu::getPressedFrameYpos(){
			return getRowLineYposOfLine(this->pressedRow)+2 - this->rowPixelHeight;
		}
		
		uint8_t MyMenu::getPressedFrameWidth(){
			return getWidth()+1;
		}
		
		uint8_t MyMenu::getPressedFrameHeight(){
			return this->rowPixelHeight-2;
		}
		
		
		void MyMenu::setPressed(uint8_t firstKey){
			switch(firstKey){
				case 'A':
					this->pressedRow = 1;
					return;
				case 'B':
					this->pressedRow = 2;
					return;
				case 'C':
					this->pressedRow = 3;
					return;
				case 'D':
					this->pressedRow = 4;
					return;
					
			}
			this->pressedRow = 0;
		}
		

	
void MyMenu::printStatus(){
	Serial.print("left:");
	Serial.println(getLeft());
	Serial.print("right:");
	Serial.println(getRight());
	Serial.print("top:");
	Serial.println(getTop());
	Serial.print("bottom:"); 
	Serial.println(getBottom());
	Serial.print("getPressedFrameXpos:"); 
	Serial.println(getPressedFrameXpos());
	Serial.print("getPressedFrameYpos:"); 
	Serial.println(getPressedFrameYpos());
	Serial.print("getPressedFrameWidth:"); 
	Serial.println(getPressedFrameWidth());
	Serial.print("getPressedFrameHeight:"); 
	Serial.println(getPressedFrameHeight());
	Serial.print("this.margins:");
	Serial.println(this->margins);
	Serial.print("this.collumnPixelWidth:");
	Serial.println(this->collumnPixelWidth);
	Serial.print("this.collumns:");
	Serial.println(this->collumns);
	Serial.print("this.pressedRow:");
	Serial.println(this->pressedRow);
	Serial.print(":");
}


//----------------------------------------------------------------



StatusPanel::StatusPanel(){
	this->visible = true;
	this->batteryVisible = true;
	this->batteryPower = 75;
	this->signalVisible = true;
	this->signalPower = 91;
}

StatusPanel::~StatusPanel(){
	bool visible;
		
		bool batteryVisible;
		uint8_t batteryPower;
		bool signalVisible;
		uint8_t signalPower;
}

bool 	StatusPanel::getVisible(){
	return this->visible;
}

void 	StatusPanel::setVisible(bool visible){
	this->visible = visible;
}
		
		
bool 	StatusPanel::getBatteryVisible(){
	return this->batteryVisible;
}

void 	StatusPanel::setBatteryVisible(bool visible){
	this->batteryVisible = visible;
}

uint8_t StatusPanel::getBatteryPower(){
	return this->batteryPower;
}

void StatusPanel::setBatteryPower(uint8_t power){
	this->batteryPower = power;
}


bool 	StatusPanel::getSignalVisible(){
	return this->signalVisible;
}

void 	StatusPanel::setSignalVisible(bool visible){
	this->signalVisible = visible;
}

uint8_t StatusPanel::getSignalPower(){
	return this->signalPower;
}

void StatusPanel::setSignalPower(uint8_t power){
	this-> signalPower = power;
}



		

