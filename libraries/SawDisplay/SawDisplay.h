#ifndef __SAWDISPLAY_H__
#define __SAWDISPLAY_H__

#include <Arduino.h>
#include "U8glib.h"

#define DISPLAY_WIDTH 	128
#define DISPLAY_HEIGHT	64
#define MAX_MENU_COLLUMNS	7

class MyMenu {
	public:
		MyMenu();
		~MyMenu();
		
		void setVisible(bool visible);
		bool getVisible();
		void setCollumns(uint8_t collumns);
		
		void setMenu1(String menu);
		void setMenu2(String menu);
		void setMenu3(String menu);
		void setMenu4(String menu);
		void setMenu(String menu1, String menu2, String menu3, String menu4, uint8_t collumns=2);
		String getMenu1();
		String getMenu2();
		String getMenu3();
		String getMenu4();
		
		uint8_t getLeft();
		uint8_t getTop();
		uint8_t getRight();
		uint8_t getBottom();
		uint8_t getHeight();
		uint8_t getWidth();
		uint8_t getCollumns();
		bool isPressed();
		uint8_t getPressedFrameXpos();
		uint8_t getPressedFrameYpos();
		uint8_t getPressedFrameWidth();
		uint8_t getPressedFrameHeight();
		void setPressed(uint8_t firstKey);
				
		uint8_t getTextXpos();
		uint8_t getTextYposOfLine(uint8_t row);
		uint8_t getRowLineYposOfLine(uint8_t row);
		void printStatus();
		
	private:
		bool visible;
		String menu1;
		String menu2;
		String menu3;
		String menu4;
		uint8_t pressedRow;
		uint8_t collumns;
		uint8_t rowPixelHeight;
		uint8_t collumnPixelWidth;
		uint8_t fontPixelHeight;
		uint8_t fontPixelWidth;
		uint8_t margins;
		
};

class StatusPanel {
	public:
		StatusPanel();
		~StatusPanel();
		
		bool 	getVisible();
		void 	setVisible(bool visible);
		
		bool 	getBatteryVisible();
		void 	setBatteryVisible(bool visible);
		uint8_t getBatteryPower();
		void setBatteryPower(uint8_t power);
		
		bool 	getSignalVisible();
		void 	setSignalVisible(bool visible);
		uint8_t getSignalPower();
		void setSignalPower(uint8_t power);
		
		
		
	private:
		bool visible;
		
		bool batteryVisible;
		uint8_t batteryPower;
		bool signalVisible;
		uint8_t signalPower;
	
};

class SawDisplay {
  
  public:
   // SawDisplay(uint8_t sck, uint8_t mosi, uint8_t cs);
	SawDisplay(MyMenu * myMenu, StatusPanel * statusPanel, uint8_t sck, uint8_t mosi, uint8_t cs);
    ~SawDisplay();
	uint8_t draw();
	void drawMenu();
	void drawStatusPanel();
	uint8_t drawMainFrame();
  private:
	U8GLIB_ST7920_128X64_1X  *display;
	MyMenu *myMenu;
	StatusPanel * statusPanel;
	
	uint8_t munuPixelWidth;
	uint8_t munuPixelHeight;
	uint8_t statusPanelPixelWidth;
	uint8_t statusPanelPixelHeight;
	uint8_t mainFramePixelWidth;
	uint8_t	mainFramePixelHeight;
	
};

#endif // __SAWDISPLAY_H__