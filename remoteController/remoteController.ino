#include <SawDisplay.h>
#include <SawKeyboard.h>
SawKeyboard sawKeyboard(5,4,3,2,6,7,8,9);
MyMenu *myMenu;
StatusPanel *statusPanel;

SawDisplay *sawDisplay;
char ch=0;
int count = 0;
void setup() {
  // put your setup code here, to run once:
  myMenu = new MyMenu();
  statusPanel = new StatusPanel();
  sawDisplay = new SawDisplay(myMenu,statusPanel, 12,11,10);
  Serial.begin(115200);
  //ESPTCPPasstroughInit();
  ESPUDPInit();
}

void loop() {
  // put your main code here, to run repeatedly:
  uint8_t result = sawKeyboard.getKey();
  //Serial.println((char)sawKeyboard.getFirstKey());
  myMenu->setPressed(sawKeyboard.getFirstKey());
  if(result > 0) {
    //Serial.println((char)result);
    String msg((char)result);
    ESPUDPSend(msg);
    //ESPTCPSend(msg);
  }
sawDisplay->draw();
count++;
if(count == 5) {
  count = 0;
  ch++;
  myMenu->setMenu1(String(ch));
}
delay(50);
 /* 
keyboard.readKeys();

keyboard.getKey()
Serial.println("bla");
*/
}


void ESPTCPPasstroughInit() {
  delay(1000);
  Serial.print("AT\r\n");
  ESPwaitForOK();//delay(1000);
  Serial.print("AT+CWMODE=1\r\n");
  ESPwaitForOK();//delay(1000);
  Serial.print("AT+CWJAP=\"PILA_KUBAS\",\"BREZA6Kubas\"\r\n");
  ESPwaitForOK();//delay(20000);
  //Serial.print("AT+CIPMUX=0\r\n");//1-multiple connections 0-single 
  //delay(1000);
  Serial.print("AT+CIFSR\r\n");
  delay(1000);
  Serial.print("AT+CIPSTART=\"TCP\",\"192.168.4.1\",333\r\n");
  delay(1000);
  Serial.print("AT+CIPMODE=1\r\n");
  delay(1000);
  Serial.print("AT+CIPSEND\r\n");
  delay(1000);
}

void ESPTCPInit() {
  delay(1000);
  Serial.print("AT\r\n");
  delay(1000);
  Serial.print("AT+CWMODE=1\r\n");
  delay(1000);
  Serial.print("AT+CWJAP=\"PILA_KUBAS\",\"BREZA6Kubas\"\r\n");
  delay(20000);
  //Serial.print("AT+CIPMUX=0\r\n");//1-multiple connections 0-single 
  //delay(1000);
  Serial.print("AT+CIFSR\r\n");
  delay(1000);
  Serial.print("AT+CIPSTART=\"TCP\",\"192.168.4.1\",333\r\n");
  delay(5000);
}

void ESPUDPInit() {
  delay(1000);
  Serial.print("AT\r\n");
  delay(1000);
  Serial.print("AT+CWMODE=1\r\n");
  delay(1000);
  Serial.print("AT+CWJAP=\"PILA_KUBAS\",\"BREZA6Kubas\"\r\n");
  delay(20000);
  //Serial.print("AT+CIPMUX=0\r\n");//1-multiple connections 0-single 
  //delay(1000);
  Serial.print("AT+CIFSR\r\n");
  delay(1000);
  Serial.print("AT+CIPSTART=\"UDP\",\"192.168.4.1\",333,1112\r\n");
  delay(5000);
}

void ESPUDPSend(String msg){
  String data("AT+CIPSEND=" + String(msg.length()+4) + ",\"192.168.4.1\",333\r\n");
  Serial.print(data);
  delay(50);
  Serial.print("--" + String(msg + "--"));
}


void ESPTCPSend(String msg){
  String data("AT+CIPSEND=" + String(msg.length()+2) + "\r\n");
  Serial.print(data);
  delay(50);
  Serial.print(String(msg + "--"));
}

void ESPwaitForOK(){
  char *waitFor= "OK\r\n";
  uint8_t count=0;
  while(1){
    delay(50);
    if(Serial.available()){
      if((char)Serial.read() == waitFor[count]){
          count++;
          if(count>1) break;
      } else {
          count==0;
      }
    }
    delay(50);
  }
}
